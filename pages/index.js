import { useEffect, useState } from "react";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import ChatRoom from "../components/chat-room";
// initialization
if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyBcDPYdgxtJfMtAyddcnvgNo3sfL9n7xCI",
    authDomain: "chat-demo-7b285.firebaseapp.com",
    projectId: "chat-demo-7b285",
    storageBucket: "chat-demo-7b285.appspot.com",
    messagingSenderId: "1093052785848",
    appId: "1:1093052785848:web:563b295b3d9d8f0f96804d",
    measurementId: "G-6V9TERE3HV",
  });
} else {
  firebase.app(); // if already initialized, use that one
}

const auth = firebase.auth();
const db = firebase.firestore();

export default function Home() {
  const [user, setUser] = useState(undefined);
  useEffect(() => {
    signInWithGoogle();
    auth.onAuthStateChanged((user) => {
      if (user) {
        setUser(user);
      } else {
        setUser(null);
      }
    });
  }, []);
  return (
    <div className="container">
      {user ? (
        <>
          <nav id="sign_out">
            <button onClick={signOut}>Sign Out</button>
          </nav>
          <ChatRoom db={db} user={user} />
        </>
      ) : (
        <section id="sign_in">
          <h1>Welcome to Chat Room</h1>
          <button onClick={signInWithGoogle}>Sign In With Google</button>
        </section>
      )}
    </div>
  );
}

const signINWithEmail = async () => {
  const provider = new firebase.auth.signINWithEmail();
  auth.useDeviceLanguage();

  try {
    await auth.signInWithPopup(provider);
  } catch (error) {
    console.log(error);
  }
};
const signInWithGoogle = async () => {
  const provider = new firebase.auth.GoogleAuthProvider();
  auth.useDeviceLanguage();

  try {
    await auth.signInWithPopup(provider);
  } catch (error) {
    console.log(error);
  }
};

const signOut = async () => {
  try {
    await firebase.auth().signOut();
  } catch (error) {
    console.log(error.message);
  }
};
